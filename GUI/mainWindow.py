#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  =================================================
# Trigger
#   - Author jouke hijlkema <jouke.hijlkema@onera.fr>
#   - dim. août 14:24 2017
#   - Initial Version 1.0
#  =================================================
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, GObject, Gdk, GdkPixbuf, Gio, GLib

import sys
sys.path.append("../GPS")
import os
from GPS.gtkNavit import gtkNavit

sys.path.append("../Terminal")
from Terminal import Terminal

import dbus
import configparser
from blinker import signal
import pulsectl

import svgutils.transform as sg
from lxml import etree

tmp_config = configparser.ConfigParser()
tmp_config.read("/home/pi/rpiCarGps/rpiCarGps.cfg")

for i in eval(tmp_config.get("Items","Active")):
    exec("from .Parts.{item} import {item}".format(item=i))

class mainWindow(Gtk.Window):
    mode   = "Day"
    items  = {}
        
    def __init__(self,config):
        "docstring"
        super(mainWindow, self).__init__()
        print("gtk version = %s.%s.%s"%(Gtk.get_major_version(),Gtk.get_minor_version(),Gtk.get_micro_version()))

        self.builder = Gtk.Builder()
        self.builder.add_from_file("/home/pi/rpiCarGps/GUI/Gui.glade")
        
        handlers = {
            "onDeleteWindow"      : self.Quit,
            "onQuitButtonPressed" : self.Quit,
            "onToggleNightDay"    : self.toggleNightDay,
            "Action"              : self.Action,
            "focusTab"            : self.focusTab,
            "levelResetX"         : self.levelResetX,
            "levelResetY"         : self.levelResetY,
            "levelZoom"           : self.levelZoom
        }

        self.builder.connect_signals(handlers)
        self.config = config
        W = self.config.getint("Items","Width")
        H = self.config.getint("Items","Height")
        W2 = self.config.getint("Items","GpsWidth")
        W1 = W-W2
        window   = self.builder.get_object("mainWindow")
        if self.config.getboolean("Items","Fullscreen"):
            window.fullscreen()
        else:
            window.set_default_size(W,H)
            window.set_size_request(W,H)
        navCont  = self.builder.get_object("navitContainer")
        myNavit  = gtkNavit(None,W2,H-65)
        navCont.add(myNavit)

        ## Counters
        butCont = self.builder.get_object("buttonContainer")
        
        for i in eval(self.config.get("Items","Active")):
            print("doing %s"%i)
            args=["w=%s"%W1]
            for kw in self.config.items(i):
                args.append("{key}={val}".format(key=kw[0],val=kw[1]))
            
            self.items[i]  = eval("{name}(self,{arguments})".format(
                name          = i,
                arguments     = ",".join(args)
            ))
            ##butCont.add(self.items[i])
            butCont.pack_start(self.items[i],True,True,0)

        self.setStyle("/home/pi/rpiCarGps/GUI/Styles/%s/dayStyles.css"%self.config.get("Items","Config"))
        
        self.toRadio = signal("toRadio")
        self.fromRadio = signal("fromRadio")
        self.fromRadio.connect(self.gotRadioSignal)

        ## Level stuff
        self.toLevel = signal("toLevel")
        self.fromLevel = signal("fromLevel")
        self.fromLevel.connect(self.gotLevelData)

        self.levelSvg  = etree.parse("/home/pi/rpiCarGps/GUI/Images/Level.svg")
        self.levelRoot = self.levelSvg.getroot()
        self.levelXi   = self.levelRoot.xpath("//*[@id = 'Xindicator']")[0]
        self.levelYi   = self.levelRoot.xpath("//*[@id = 'Yindicator']")[0]
        self.levelFL   = self.levelRoot.xpath("//*[@id = 'wheel-FL']")[0]
        self.levelFR   = self.levelRoot.xpath("//*[@id = 'wheel-FR']")[0]
        self.levelRL   = self.levelRoot.xpath("//*[@id = 'wheel-RL']")[0]
        self.levelRR   = self.levelRoot.xpath("//*[@id = 'wheel-RR']")[0]

        ## Radio stuff
        self.Store = False

        # Action signal to talk to main proc
        self.actionSignal = signal("Actions")

        # MPD stuff
        self.toMpd   = signal("toMPD")
        self.fromMpd = signal("fromMPD")
        self.fromMpd.connect(self.gotMpdData)
        rendererText = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Artist", rendererText, text=0)
        self.builder.get_object("mpdTree").append_column(column)
        self.builder.get_object("mpdTree").connect("row-activated", self.mpdPlaySong)

        # Volume stuff
        GLib.timeout_add(200, self.checkVolumeData)
        
        self.updateLevelSvg()

        # Terminal stuff
        self.term = Terminal.Terminal()
        self.builder.get_object("termTab").pack_start(self.term,True,True,0)
        
        window.show_all()
        myNavit.start()

    ## --------------------------------------------------------------
    ## Description : zoom/unzoom level
    ## NOTE : 
    ## -
    ## Author : jouke hylkema
    ## date   : 30-42-2020 10:42:05
    ## --------------------------------------------------------------
    def levelZoom(self, args):
        self.config["Level"]["zoom"]="%s"%self.builder.get_object("levelZoom").get_value()
        
    ## --------------------------------------------------------------
    ## Description : reset level
    ## NOTE : 
    ## -
    ## Author : jouke hylkema
    ## date   : 29-25-2020 20:25:53
    ## --------------------------------------------------------------
    def levelResetX(self, args):
        self.toLevel.send("resetX")
    def levelResetY(self, args):
        self.toLevel.send("resetY")
        
    ## --------------------------------------------------------------
    ## Description : update level svg
    ## NOTE : 
    ## -
    ## Author : jouke hylkema
    ## date   : 29-04-2020 18:04:42
    ## --------------------------------------------------------------
    def updateLevelSvg(self):
        self.builder.get_object("levelImage").clear()
        stream = Gio.MemoryInputStream.new_from_bytes(GLib.Bytes.new(etree.tostring(self.levelSvg)))
        pixbuf = GdkPixbuf.Pixbuf.new_from_stream(stream, None)
        self.builder.get_object("levelImage").set_from_pixbuf(pixbuf)
        
        
    ## --------------------------------------------------------------
    ## Description : tab focus changed
    ## NOTE : 
    ## -
    ## Author : jouke hylkema
    ## date   : 29-51-2020 15:51:51
    ## --------------------------------------------------------------
    def focusTab(self, nb,box,i):
        if (i==1):
            self.toLevel.send("start")
            print("W=%s,H=%s"%(nb.get_allocation().width,nb.get_allocation().height))
            print("W=%s,H=%s"%(box.get_allocation().width,box.get_allocation().height))
        else:
            self.toLevel.send("stop")

    ## --------------------------------------------------------------
    ## Description : treat mpd signals
    ## NOTE : 
    ## -
    ## Author : jouke hylkema
    ## date   : 13-06-2021 12:06:14
    ## --------------------------------------------------------------
    def gotMpdData(self, data):
        # print("MPD: received %s"%data)
        if "action" in data:
            if "play" in data["action"]:
                GLib.idle_add(self.builder.get_object("mpdArtist").set_text,data["artist"])
                GLib.idle_add(self.builder.get_object("mpdAlbum").set_text,data["album"])
                GLib.idle_add(self.builder.get_object("mpdTitle").set_text,data["title"])
            if "update" in data["action"] and "title" in data:
                GLib.idle_add(self.builder.get_object("mpdArtist").set_text,data["artist"])
                GLib.idle_add(self.builder.get_object("mpdAlbum").set_text,data["album"])
                GLib.idle_add(self.builder.get_object("mpdTitle").set_text,data["title"])
            
                GLib.idle_add(self.items["musicPanel"].song.set_label,data["title"])
                GLib.idle_add(self.items["musicPanel"].artist.set_label,data["artist"])
    def mpdPlaySong(self, widget, row, col):
        self.toMpd.send("fromTree;%s"%row)
    ## --------------------------------------------------------------
    ## Description : treat volume signals
    ## NOTE : 
    ## -
    ## Author : jouke hylkema
    ## date   : 10-01-2021 18:01:42
    ## --------------------------------------------------------------
    def checkVolumeData(self):
        with pulsectl.Pulse('volume-increaser') as pulse:
            # print(pulse.sink_list())
            sink   = pulse.sink_list()[1]
            volume = pulse.volume_get_all_chans(sink)
            # print(100*volume)
            GLib.idle_add(self.builder.get_object("volumeLevel").set_value,100*volume)
        return True
        
    ## --------------------------------------------------------------
    ## Description : treat Level data
    ## NOTE : somehow X and Y are inversed
    ## -
    ## Author : jouke hylkema
    ## date   : 29-17-2020 16:17:00
    ## --------------------------------------------------------------
    def gotLevelData(self, data):

        # print("Level")
        # print(data)
        if "raz" in data:
            pass
        else:
            S = pow(10,float(self.config["Level"]["zoom"]))
            X = round(S*0.0631*data["X"]+265)
            Y = round(S*0.1071*data["Y"]+450)
            # print("X:%s, Y:%s"%(X,Y))
            self.levelXi.set("y1","%s"%X)
            self.levelXi.set("y2","%s"%X)
            
            self.levelYi.set("x1","%s"%Y)
            self.levelYi.set("x2","%s"%Y)

            threshold = 3
            Xmin = 265 - threshold
            Xmax = 265 + threshold
            Ymin = 450 - threshold
            Ymax = 450 + threshold
            
            self.levelRL.set("style","opacity:1;fill:black;stroke:black;stroke-width:7.55906")
            self.levelRR.set("style","opacity:1;fill:black;stroke:black;stroke-width:7.55906")
            self.levelFL.set("style","opacity:1;fill:black;stroke:black;stroke-width:7.55906")
            self.levelFR.set("style","opacity:1;fill:black;stroke:black;stroke-width:7.55906")

            if X < Xmin:
                self.levelFL.set("style","opacity:1;fill:red;stroke:black;stroke-width:7.55906")
                self.levelFR.set("style","opacity:1;fill:red;stroke:black;stroke-width:7.55906")
            elif X > Xmax:
                self.levelRL.set("style","opacity:1;fill:red;stroke:black;stroke-width:7.55906")
                self.levelRR.set("style","opacity:1;fill:red;stroke:black;stroke-width:7.55906")
            if Y < Ymin:
                self.levelFR.set("style","opacity:1;fill:red;stroke:black;stroke-width:7.55906")
                self.levelRR.set("style","opacity:1;fill:red;stroke:black;stroke-width:7.55906")
            elif Y > Ymax:
                self.levelFL.set("style","opacity:1;fill:red;stroke:black;stroke-width:7.55906")
                self.levelRL.set("style","opacity:1;fill:red;stroke:black;stroke-width:7.55906")
                
            GLib.idle_add(self.updateLevelSvg)
        
    ## --------------------------------------------------------------
    ## Description : treat signal received from radio
    ## NOTE : 
    ## -
    ## Author : jouke hylkema
    ## date   : 28-31-2019 11:31:54
    ## --------------------------------------------------------------
    def gotRadioSignal (self,data):
        # print("GUI: received %s from Radio"%data)
        # for i in data:
        #     print("%s: %s"%(i,data[i]))
        GLib.idle_add(self.builder.get_object("radioFreq").set_text,
                      "%s MHz"%data["Channel"] if "Channel" in data else "")
        GLib.idle_add(self.builder.get_object("radioInfo").set_text,
                      "%s"%data["Station"] if "Station" in data else "")
        GLib.idle_add(self.builder.get_object("radioSignalStrength").set_value,
                      float(data["Strength"]))
        rDS = ""
        for c in data["rdsText"]:
            rDS+=c
        GLib.idle_add(self.builder.get_object("radioRdsText").set_text,rDS)
            
    ## --------------------------------------------------------------
    ## Description : GUI actions
    ## NOTE : 
    ## -
    ## Author : jouke hylkema
    ## date   : 26-06-2019 14:06:35
    ## --------------------------------------------------------------
    def Action (self,args,args2=None):
        print("mainwindow Action: %s"%args.get_name())
        source = args.get_name()
        if "seekUp" in source:
            self.toRadio.send("seekUp")
            # self.clearRadioData()
        elif "seekDown" in source:
            self.toRadio.send("seekDown")
            # self.clearRadioData()
        elif "radioOnOff" in source:
            self.actionSignal.send("radioOnOff")
            # self.clearRadioData()
        elif "radioStoreSelector" in source:
            self.Store = self.builder.get_object("radioStoreSelector").get_active ()
        elif "radioStore" in source:
            id = source.split("_")[1]
            if self.Store:
                self.actionSignal.send("radioStore %s"%id)
            else:
                self.actionSignal.send("radioRestore %s"%id)
        elif "mpdPlay" in source:
            #print("MPD play")
            self.toMpd.send("play")
        elif "mpdSkip" in source:
            self.toMpd.send("next")
        elif "mpdPrev" in source:
            self.toMpd.send("previous")
        elif "actionsScreen" in source:
            os.system("xset -display :0 s blank")
            os.system("xset -display :0 s reset")
            os.system("xset -display :0 s activate")
        elif "shutdownButton" in source:
            self.actionSignal.send("save")
            os.system("sudo poweroff")
        elif "goodSpotButton" in source:
            self.actionSignal.send("goodSpot")
        

    ## --------------------------------------------------------------
    ## Description : empty radio data
    ## NOTE : 
    ## -
    ## Author : jouke hylkema
    ## date   : 14-56-2021 17:56:06
    ## --------------------------------------------------------------
    def clearRadioData(self):
        print("clear radio data")
        GLib.idle_add(self.builder.get_object("radioFreq").set_text,"")
        GLib.idle_add(self.builder.get_object("radioInfo").set_text,"")
        GLib.idle_add(self.builder.get_object("radioRdsText").set_text,"")
    ## --------------------------------------------------------------
    ## Description : set the style
    ## NOTE : 
    ## -
    ## Author : jouke hylkema
    ## date   : 22-52-2017 13:52:36
    ## --------------------------------------------------------------
    def setStyle (self,path):
        self.myCss = Gtk.CssProvider()
        self.myCss.load_from_path(path)
        Gtk.StyleContext.add_provider_for_screen(
            Gdk.Screen.get_default(), 
            self.myCss,     
            Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
        )

    ## --------------------------------------------------------------
    ## Description : Quit
    ## NOTE : 
    ## -
    ## Author : jouke hylkema
    ## date   : 20-26-2017 14:26:50
    ## --------------------------------------------------------------
    def Quit (self,who):
        Gtk.main_quit()

    ## --------------------------------------------------------------
    ## Description : toggle night and day mode
    ## NOTE : 
    ## -
    ## Author : jouke hylkema
    ## date   : 20-27-2017 14:27:11
    ## --------------------------------------------------------------
    def toggleNightDay (self,who):
        bus     = dbus.SessionBus()
        object  = bus.get_object("org.navit_project.navit","/org/navit_project/navit/default_navit")
        iface   = dbus.Interface(object,dbus_interface="org.navit_project.navit")
        iter    = iface.attr_iter()
        path    = object.get_attr_wi("navit",iter)
        navit   = bus.get_object('org.navit_project.navit', path[1])
        #print(navit)
        iface.attr_iter_destroy(iter)
        
        if self.mode=="Day":
            #print("switch to night")
            navit.set_layout("Car-dark")
            self.setStyle("GUI/Styles/%s/nightStyles.css"%self.config.get("Items","Config"))
            self.mode  = "Night"
        else:
            #print("switch to day")
            navit.set_layout("Car")
            self.setStyle("GUI/Styles/%s/dayStyles.css"%self.config.get("Items","Config"))
            self.mode  = "Day"
