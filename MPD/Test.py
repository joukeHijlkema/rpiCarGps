#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  =================================================
#   - Author jouke hijlkema <jouke.hijlkema@onera.fr>
#   - lun. sept. 10:11 2022
#   - Initial Version 1.0
#  =================================================
from mpd import MPDClient
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, GObject, Gdk, GdkPixbuf, Gio, GLib

class PyApp(Gtk.Window):
    def __init__(self):
        super(PyApp, self).__init__()
        self.set_title("TreeView with ListStore")
        self.set_default_size(250, 200)

        client = MPDClient()
        client.connect("localhost", 6600)
        client.clear()
        client.load("All")
        client.shuffle()
        
        store = Gtk.TreeStore(str)

        for a in client.list("artist"):
            row = store.append(None, [a["artist"]])
            for p in client.list("album",a["artist"]):
                plaat = store.append(row,[p["album"]])
                for s in client.list("title","artist",a["artist"],"album",p["album"]):
                    store.append(plaat,[s["title"]])
      
        treeView = Gtk.TreeView()
        treeView.set_model(store)

        rendererText = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Artist", rendererText, text=0)
        treeView.append_column(column)
                
        treeView.connect("row-activated", self.on_activated)

        root = Gtk.ScrolledWindow()
        root.add(treeView)
        self.add(root)
        
        self.connect("destroy", Gtk.main_quit)
        self.show_all()
      
    def on_activated(self, widget, row, col):
       
        print("clicked row:",row,",col:",col)
          
def main():
    Gtk.main()
    return

if __name__ == "__main__":
    bcb = PyApp()
    main()
